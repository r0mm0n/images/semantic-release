FROM  registry.gitlab.com/r0mm0n/images/node:20.14.0
RUN npm install --global \
    semantic-release \
    @semantic-release/changelog \
    @semantic-release/git \
    @semantic-release/gitlab \
    semantic-release-replace-plugin \
    conventional-changelog-conventionalcommits \
    @semantic-release/exec \
    && npm cache clean --force

